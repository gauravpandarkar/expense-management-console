package group;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import createdashboard.CreateDashBoard;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;

public class GroupController {
	@FXML
	private TextField groupName;
	@FXML
	private TextField groupType;
	@FXML
	private Button createGroup;
	@FXML
	private Button back;

	public void createGroup(ActionEvent event) throws IOException {
		if (groupName.getText().isEmpty() || groupType.getText().isEmpty()) {
			Alert alert = new Alert(Alert.AlertType.ERROR);
			alert.setTitle("Error");
			alert.setHeaderText(null);
			alert.setContentText("Please fill in all fields.");
			alert.showAndWait();
			return;
		}

		String request = "{\n" + "\"groupName\"" + ":\"" + groupName.getText() + "\", \r\n" + "\"groupType\"" + ":\""
				+ groupType.getText() + "\" \r\n" + "\n}";

		System.out.println(request);

		String url = "http://localhost:8080/groups/api/v1/create";
		URL urlObj = new URL(url);
		HttpURLConnection postConnection = (HttpURLConnection) urlObj.openConnection();
		postConnection.setRequestMethod("POST");

		postConnection.setRequestProperty("Content-Type", "application/json");
		postConnection.setDoOutput(true);

		OutputStream osObj = postConnection.getOutputStream();
		osObj.write(request.getBytes());

		osObj.flush();
		osObj.close();
		int respCode = postConnection.getResponseCode();
		System.out.println("Response from the server is: \n");
		System.out.println("The POST Request Response Code :  " + respCode);
		System.out.println("The POST Request Response Message : " + postConnection.getResponseMessage());
		if (respCode == HttpURLConnection.HTTP_CREATED) {

			InputStreamReader inputStreamobj = new InputStreamReader(postConnection.getInputStream());
			BufferedReader br = new BufferedReader(inputStreamobj);
			String input = null;
			StringBuffer streamBuffer = new StringBuffer();
			while ((input = br.readLine()) != null) {
				streamBuffer.append(input);
			}
			br.close();
			postConnection.disconnect();

			System.out.println(streamBuffer.toString());
			new CreateDashBoard().show();

		} else {

			System.out.println("POST Request did not work.");
			Alert alert = new Alert(Alert.AlertType.ERROR);
			alert.setTitle("Error");
			alert.setHeaderText(null);
			alert.setContentText("POST Request did not work.");
			alert.showAndWait();
			return;

		}
	}

	public void back(ActionEvent event) {
		new CreateDashBoard().show();

	}

}
