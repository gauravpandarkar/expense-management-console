package createdashboard;

import group.Group;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import login.Login;

public class CreateDashBoardController {
	@FXML
	private Button createGroup;
	@FXML
	private Button createFriend;
	@FXML
	private Button back;

	public void createGroup(ActionEvent event) {
		new Group().show();

	}

	public void createFriend(ActionEvent event) {

	}

	public void back(ActionEvent event) {
		new Login().show();

	}

}
