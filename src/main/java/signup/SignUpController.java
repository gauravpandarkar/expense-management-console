package signup;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;

import java.net.URL;
import java.util.ResourceBundle;

import dashboard.DashBoard;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;

public class SignUpController implements Initializable {
	@FXML
	private TextField fullName;
	@FXML
	private TextField mobile;
	@FXML
	private TextField email;
	@FXML
	private PasswordField password;
	@FXML
	private ComboBox currency;
	@FXML
	private ComboBox country;
	@FXML
	private ComboBox language;
	@FXML
	private Button back;
	@FXML
	private Button signUp;

	public void back(ActionEvent event) {
		new DashBoard().show();

	}

	public void signUp(ActionEvent event) throws IOException {
		if (fullName.getText().isEmpty() || mobile.getText().isEmpty() || email.getText().isEmpty() || password.getText().isEmpty()) {
			Alert alert = new Alert(Alert.AlertType.ERROR);
			alert.setTitle("Error");
			alert.setHeaderText(null);
			alert.setContentText("Please fill in all fields.");
			alert.showAndWait();
			return;
		}
		
		final String messageContent = "{\n" + "\"fullName\"" + ":\"" + fullName.getText() + "\", \r\n" + "\"email\""
				+ ":\"" + email.getText() + "\", \r\n" + "\"mobileNo\"" + ":\"" + mobile.getText() + "\", \r\n"
				+ "\"password\"" + ":\"" + password.getText() + "\", \r\n" + "\"country\"" + ":\"" + country.getValue()
				+ "\", \r\n" + "\"currency\"" + ":\"" + currency.getValue() + "\", \r\n" + "\"language\"" + ":\""
				+ language.getValue() + "\" \r\n" + "\n}";

		System.out.println(messageContent);

		String urlApi = "http://localhost:8080/directory/api/v1/signup";
		URL url = new URL(urlApi);
		HttpURLConnection postConnection = (HttpURLConnection) url.openConnection();
		postConnection.setRequestMethod("POST");
		

		postConnection.setRequestProperty("Content-Type", "application/json");
		postConnection.setDoOutput(true);

		OutputStream outputStreamobj = postConnection.getOutputStream();
		outputStreamobj.write(messageContent.getBytes());

		outputStreamobj.flush();
		outputStreamobj.close();
		int respCode = postConnection.getResponseCode();
		System.out.println("Response from the server is: \n");
		System.out.println("The POST Request Response Code :  " + respCode);
		System.out.println("The POST Request Response Message : " + postConnection.getResponseMessage());
		if (respCode == HttpURLConnection.HTTP_CREATED) {
			
			InputStreamReader inputStreamobj = new InputStreamReader(postConnection.getInputStream());
			BufferedReader br = new BufferedReader(inputStreamobj);
			String input = null;
			StringBuffer stringBuffer = new StringBuffer();
			while ((input = br.readLine()) != null) {
				stringBuffer.append(input);
			}
			br.close();
			postConnection.disconnect();
			System.out.println(stringBuffer.toString());

		} else {
			System.out.println("POST Request did not work.");
			Alert alert = new Alert(Alert.AlertType.ERROR);
			alert.setTitle("Error");
			alert.setHeaderText(null);
			alert.setContentText("POST Request did not work.");
			alert.showAndWait();
			return;
		}

	}

	@Override
	public void initialize(URL location, ResourceBundle resources) {

		ObservableList<String> list = FXCollections.observableArrayList("Dollar", "Rupee");
		currency.setItems(list);
		ObservableList<String> list1 = FXCollections.observableArrayList("India", "Russia", "America", "Shrilanka",
				"Aushtrelia", "Japan", "Europ", "NewZeland", "England");
		country.setItems(list1);
		ObservableList<String> list2 = FXCollections.observableArrayList("Marathi", "English", "Hindi");
		language.setItems(list2);

	}

}
